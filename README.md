"Data-Table" 

1. Able to sort
2. Able to perform caseInsensitive filter
3. Able to perform pagination
4. Seperate the displayed content and the actual data to allow user the freedom to render anything they want

todo: sort and filter requires repaint of the view at the moment.

Demo:
[<strong><a href="https://cdn.rawgit.com/wangx6/data-table/master/index.html">Data Table Demo</a></strong>]


![ScreenShot](https://raw.github.com/wangx6/data-table/master/data-table-screenshot.jpg)
