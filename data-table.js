var DataTable = (function() {
    'use strict';

    var dh = {
        makeEle: function(type, options) {
            var except = ['innerText', 'innerHTML', 'className'];
            var ele = document.createElement(type);
            for (var i in options) {
                if (except.indexOf(i) > -1) ele[i] = options[i];
                else ele.setAttribute(i, options[i]);
            }
            return ele;
        }
    };

    var dataHandler = {
        /**
         * find data from deep path
         * @param {}
         */
        find: function(path, data) {
            return (function(steps, data, isEmpty) {
                for (var i = 0, ln = steps.length; i < ln; i++) {
                    data = data[steps[i]];
                    if (isEmpty(data)) return undefined;
                }
                return data;
            })(path.replace(/\s+/g, '').split('.').slice(0), data, this.isEmpty);
        },

        /**
         * Check if the data is undefined
         * @param {}
         */
        isEmpty: function(d) {
            return d === void 0;
        },
        isArray: function(v) {
            return Object.prototype.toString.call(v) === '[object Array]';
        },
        isStrInNumFormat: function(val) {
            return val.match(regex.isNum);
        }
    };

    var Observer = function() {
        this.cache = {};

        this.trigger = function(topic, args) {
            if (!this.cache[topic]) return;
            for (var i = 0, el; el = this.cache[topic][i]; i++) el.fn.apply(el.scope, args || []);
        };

        this.on = function(topic, callback, scope) {
            if (!this.cache[topic]) this.cache[topic] = [];
            this.cache[topic].push({ fn: callback, scope: scope || callback });
            return [topic, callback, scope];
        };

        this.off = function(handle) {
            var i = 0,
                el, topic = handle[0];
            if (!this.cache[topic]) return;
            for (; el = this.cache[topic][i]; i++)
                if (el.fn == handle[1]) this.cache[topic].splice(i, 1);
        };
    };

    var brain = new Observer();
    var regex = {
        isNum: /^[0-9]*(.?[0-9]+)?$/g,
    };

    /**
     * Data Table
     * @class
     * @param {}
     */
    var DataTable = function(config) {
        this.config = config || {};
        this.observer = new Observer();
        this.baseCls = 'data-table';
        this.parent = config.parent;
        this.ele = null;
        this.topEle = null;
        this.middleEle = null;
        this.bottomEle = null;
        this.table = null;
        this.thead = null;
        this.tbody = null;
        this.filterEle = null;
        this.mask = dh.makeEle('div', { className: this.baseCls + '__mask' });

        this.data = this.config.data || null;
        this.active = null;
        this.groupData = this.config.groupData;
        this.pool = null;
        this.rows = [];
        this.headers = [];
        this.isLoaded = false;
        this.pagination = null;
        this.hasPagination = this.config.hasPagination;
        this.hasFilter = this.config.hasFilter || false;
        this.paginationSize = this.config.paginationSize || 20;
        this.init();
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.init = function() {
        this.initHtml(); 
        if (this.groupData) this.updateGroupData();
        if (this.data) this.updateData(this.data);
        this.initSubscription();
        this.initEvent();
    };

    DataTable.prototype.initHtml = function() {
        this.ele = dh.makeEle('div', { className: this.baseCls + '-container' });
        this.topEle = dh.makeEle('div', { className: this.baseCls + '-top' });
        this.middleEle = dh.makeEle('div', { className: this.baseCls + '-middle' });
        this.bottomEle = dh.makeEle('div', { className: this.baseCls + '-bottom' });
        this.table = dh.makeEle('table', { className: this.baseCls + '__table' });
        this.tbody = dh.makeEle('tbody', { className: this.baseCls + '-tbody' });
        this.thead = dh.makeEle('thead', { className: this.baseCls + '-thead' });
        this.filterEle = dh.makeEle('input', {className: this.baseCls + '__filter-input', placeholder: 'Search...'});

        this.ele.appendChild(this.topEle);
        this.ele.appendChild(this.middleEle);
        this.ele.appendChild(this.bottomEle);
        this.middleEle.appendChild(this.table);
        this.table.appendChild(this.thead);
        this.table.appendChild(this.tbody);

        if(this.hasFilter) this.topEle.appendChild(this.filterEle);
        if(this.parent) this.parent.appendChild(this.ele);
    };   

    DataTable.prototype.initEvent = function() {
        var me = this,
        d, data;
        
        this.filterEle.addEventListener('keyup', function(e) {
            var val = (e.target.value + '').toLowerCase(),
                filtered = [],
                i, reg, found;

            if(val === '') return me.setActive(me.data);
            
            filtered = me.data.filter(function(d) {
                found = false;
                for(i in d) {
                    if((d[i] + '').toLowerCase().match(new RegExp(val))) {
                        found = true;
                        break;
                    }
                }
                return found;
            });
            me.setActive(filtered);
        });
    };

    DataTable.prototype.processData = function() {
        // 1 check filter
        // 2 check sorter
        // 3 check pagination
        // 4 return active data
        //this.active = this.data.slice(0, this.paginationSize);
        var data = this.data;
        var i = 0, ln = data.length;
        for(; i < ln; i++) {
            data[i].__index__ = i;
        }
        return this.data;
    };

    DataTable.prototype.initSubscription = function() {
        brain.on('clickpaginationstart', function() {
            
        });
        brain.on('clickpaginationleft', function() {
            
        });
        brain.on('clickpaginationright', function() {
            
        });

        brain.on('clickpaginationend', function() {
            
        });
    };

    DataTable.prototype.initPagination = function() {
        this.pagination = new Pagination({
            rows: this.rows,
            paginationSize: this.paginationSize
        });
        this.bottomEle.appendChild(this.pagination.ele);
      
        return this.pagination;
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.setGroupData = function(groupData) {
        this.groupData = groupData;
        this.updateGroupData(groupData);
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.setData = function(data) {
        this.data = data;
        this.updateData(data);
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.updateData = function(newData) {
        var data = this.processData();
        this.setActive(data);
    };

    DataTable.prototype.setActive = function(a) {
        this.active = a || this.data;
        this.updateActive(a);
    };

    DataTable.prototype.updateActive = function(newActive) {
        this.updateRow(newActive);
    };

     /**
     * 
     * @param {}
     */
    DataTable.prototype.updateRow = function(active) {
        var tr, i, j;
        var row;
        var frag = document.createDocumentFragment();
        var ln;
        var d;

        this.removeAll();

        for(i = 0, ln = active.length; i < ln; i++) {
            d = active[i];
            row = new DataTableRow({
                groupData: this.groupData,
            });
            row.setData(d);
            frag.appendChild(row.ele);
            this.rows.push(row);
        }
        this.tbody.appendChild(frag);
        
        if(this.hasPagination) {
            this.pagination = this.pagination || this.initPagination();
            this.pagination.setRows(this.rows);
        }
    };

    DataTable.prototype.findBy = function(prop, value) {
        var r, found = [];
        for(var i = 0, ln = this.rows.length; i < ln; i++) {
            r = this.rows[i];
            if(r.data[prop] === value) {
                found.push(r);
            }
        }
        return found;
    };

    DataTable.prototype.removeRow = function(row) {
        var index = this.rows.indexOf(row);

        if(index > -1) {
            this.removeAt(index);
        }
    };

    DataTable.prototype.removeAt = function(i) {
        var row = this.rows[i];
        var par = row.ele.parentNode;

        if(par) {
            par.removeChild(row.ele);
        }
        this.rows.splice(i, 1);
        this.data.splice(i, 1);
        this.pagination.setRows(this.rows);
    };

    DataTable.prototype.removeAll = function() {
        this.rows = [];
        this.tbody.innerHTML = '';
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.updateGroupData = function(groupData) {
        groupData = groupData || this.groupData;
        this.updateHeader(groupData);
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.updateHeader = function(groupData) {
        var tr, th, i = 0,
            ln;

        if (groupData) {
            for (ln = groupData.length; i < ln; i++) {
                th = new DataTableHeader({
                    headerData: groupData[i],
                    width: 1/ln * 100
                });
                this.thead.appendChild(th.ele);
                this.headers.push(th);
            }
        }
        this.updateHeaderEvent();
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.updateHeaderEvent = function() {
        for (var i = 0; i < this.headers.length; i++) {
            if(this.headers[i].enableSort) {
                this.headers[i].ele.addEventListener('click', this.onClickHeader.bind(this, this.headers[i]));
            }
        }
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.onClickHeader = function(header) {
        var headers = this.headers,
            i = 0, ln = headers.length, h;

        if(header.isSorted) {
            this.rows.reverse();
            this.onSortRows(this.rows);
            header.toggleDirection();
            return;
        }

        for(; i < ln; i++) {
            h = this.headers[i];
            if(h !== header) {
                h.resetArrows();
                h.setIsSorted(!1);
            }
        }
       
        this.sortRows({ prop: header.headerData.path, direction: header.direction });
        header.setIsSorted(!0);
        header.toggleDirection();
    };

    DataTable.prototype.onSortRows = function() {
        var i = 0, ln = this.rows.length;
        var frag = document.createDocumentFragment();
        for(; i < ln; i++) {
            frag.appendChild(this.rows[i].ele);
        }
        this.tbody.innerHTML = '';
        this.tbody.appendChild(frag);
        if(this.hasPagination) {
            this.pagination.setRows(this.rows);
        }
    };

    DataTable.prototype.sortRows = function(sorter) {
        var val = sorter.direction === 'ASC' ? 1 : -1;
        var i = 0, ln = this.rows.length;
        var v1, v2, i1, i2;

        this.rows.sort(function(r1, r2) {
            v1 = r1.data[sorter.prop];
            v2 = r2.data[sorter.prop];
            i1 = r1.data.__index__;
            i2 = r2.data.__index__;
            
            if(v1 === v2) {
                return i1 > i2 ? val : -val;
            }
            return v1 > v2 ? val : -val;
        });
        this.onSortRows();
    };

    /**
     * 
     * @param {}
     */
    DataTable.prototype.add = function(row) {
        this.table.appendChild(row.ele);
        this.rows.push(row);
    };


    function Pagination(config) {
        this.config = config || {};
        this.ele = dh.makeEle('div', { className: 'pagination' });
        this.btnHolder = dh.makeEle('div', { className: 'pagination-btn-holder' });

        this.left = null;
        this.right = null;
        this.start = null;
        this.end = null;
        this.infoBoard = null;
        this.pages = null;
        this.rows = this.config.rows || [];
        this.pageIndex = 0;
        this.oldPageIndex = null;
        this.paginationSize = Math.abs(this.config.paginationSize) || 10;
        this.init();
    }

    Pagination.prototype = {
        init: function() {
            this.initEle();
            this.initEvent();
            this.hideAll();
            this.splitIntoPage();
            this.updatePageIndex(this.pageIndex);
        },

        setRows: function(rows) {
            this.rows = rows;
            this.updateRows(rows);
        },

        updateRows: function(newRows) {
            this.splitIntoPage();
            this.setPageIndex(0);
            if(this.getSize() === 0) {
                this.emptyMessageDisplay();
            }
        },

        initEle: function() {
            this.ele.appendChild(this.btnHolder);
            this.btnHolder.appendChild(this.start = dh.makeEle('div', { innerText: '<<', className: 'pagination-btn' }));
            this.btnHolder.appendChild(this.left = dh.makeEle('div', { innerText: '<', className: 'pagination-btn' }));
            this.btnHolder.appendChild(this.right = dh.makeEle('div', { innerText: '>', className: 'pagination-btn' }));
            this.btnHolder.appendChild(this.end = dh.makeEle('div', { innerText: '>>', className: 'pagination-btn' }));
            this.ele.appendChild(this.infoBoard = dh.makeEle('div', { className: 'pagination-info-board' }));
        },
        initEvent: function() {
            var me = this;
            this.start.addEventListener('click', function() {
                me.setPageIndex(0);
                brain.trigger('clickpaginationstart', []);
            });
            this.left.addEventListener('click', function() {
                if(me.pageIndex === 0) return;
                me.setPageIndex(me.pageIndex - 1);
                brain.trigger('clickpaginationleft', []);
            });
            this.right.addEventListener('click', function() {
                if(me.pageIndex === me.getPageSize() - 1) return;
                me.setPageIndex(me.pageIndex + 1);
                brain.trigger('clickpaginationright', []);
            });
            this.end.addEventListener('click', function() {
                brain.trigger('clickpaginationend', []);
                me.setPageIndex(me.getPageSize() - 1);
            });
        },
        getPageSize: function() {
            return this.pages.length;
        },
        splitIntoPage: function() {
            var pages = [],
                rows = this.rows,
                i = 0, p = -1;

            for (; i < rows.length; i++) {
                if (i % this.paginationSize === 0) {
                    pages[++p] = [];
                }
                pages[p].push(rows[i]);
            }
            this.pages = pages;
            return pages;
        },
        setPageIndex: function(pageIndex) {
            if(!this.pages[pageIndex]) return;
            this.pageIndex = pageIndex;
            this.updatePageIndex(pageIndex);
        },

        updatePageIndex: function(newPageIndex) {
            for(var i = 0; i < this.getPageSize(); i++) {
                if(newPageIndex === i) {
                    this.showPage(i);
                } else{
                    this.hidePage(i);
                }
            }
            
            this.updateInfoBoard();
        },

        updateInfoBoard: function() {
            var from = this.pageIndex * this.paginationSize + 1;
            var to = from + this.pages[this.pageIndex].length - 1;
            this.infoBoard.innerText = from + ' to ' + to +' of ' + this.getSize();
        },

        emptyMessageDisplay: function() {
            this.infoBoard.innerText = '0 results found.';
        },

        hidePage: function(pageIndex) {
            var pageItems = this.pages[pageIndex];
            if(pageItems) {
                pageItems.map(function(i){
                    i.hide();
                });
            }
        },

        showPage: function(pageIndex) {
            var pageItems = this.pages[pageIndex];
            if(pageItems) {
                for(var i = 0; i < pageItems.length; i++) {
                    pageItems[i].show();   
                }
            }
        },
        
        refresh: function() {
            if (this.getSize() <= this.paginationSize) {
                this.showAll();
            } else {
                for (var i = 0; i < this.rows.length; i++) {
                    this.rows[i][this.isInRange(i) ? 'show' : 'hide']();
                }
            }
        },
        
        showAll: function() {
            for (var i = 0; i < this.rows.length; i++) {
                this.rows[i].show();
            }
        },

        hideAll: function() {
            for (var i = 0; i < this.rows.length; i++) {
                this.rows[i].hide();
            }
        },

        getSize: function() {
            return this.rows.length;
        }
    };

    /**
     * Data Table Row
     * @class
     * @param {}
     */
    function DataTableRow(config) {
        this.config = config || {};

        this.ele = null;
        this.cells = [];
        this.groupData = this.config.groupData;
        this.isLoaded = false;
        this.data = null;
        this.group = this.config.group;
        this.init();
    }

    /**
     * 
     * @param {}
     */
    DataTableRow.prototype.add = function(td) {
        this.ele.appendChild(td.ele);
        this.cells.push(td);
    };

    /**
     * 
     * @param {}
     */
    DataTableRow.prototype.init = function() {
        this.initEle();
        if (this.data) {
            this.updateData();
        }
    };

    DataTableRow.prototype.show = function() {
        this.ele.style.display = 'table-row';
    };

    DataTableRow.prototype.hide = function() {
        this.ele.style.display = 'none';
    };


    /**
     * 
     * @param {}
     */
    DataTableRow.prototype.initEle = function() {
        var me = this;
        me.ele = dh.makeEle('tr', {
            className: 'data-table__data-row'
        });
    };

    /**
     * 
     * @param {}
     */
    DataTableRow.prototype.setData = function(data) {
        this.data = data;
        this.updateData(data);
    };

    /**
     * 
     * @param {}
     */
    DataTableRow.prototype.updateData = function(newData) {
        var j, cell, ln;
        var g;
        
        if (!this.isLoaded) {
            for (j = 0, ln = this.groupData.length; j < ln; j++) {
                g = this.groupData[j];
                cell = new DataTableCell({
                    cellType: 'td',
                    groupData: g
                });
                cell.setData(newData);
                this.add(cell);
            }
            this.isLoaded = true;
        } else {
            this.refresh(newData);
        }
    };

    /**
     * 
     * @param {}
     */
    DataTableRow.prototype.refresh = function(newData) {
        var i, cell, cells = this.cells, ln = cells.length;
        newData = newData || this.data;
        if (!this.groupData.length) return;
        for (i = 0; i < ln; i++) {
            cell = cells[i];
            cell.setData(newData);
        }
    };

    /**
     * Data Table Cell 
     * @calss
     * @param {}
     */
    function DataTableCell(config) {
        this.config = config || {};
        this.config.cellType = this.config.cellType || null;
        this.config.CELL_TYPES = ['td', 'th'];
        this.config.BASE_COLOR = ['#333', 'green', 'red'];

        this.ele = null;
        this.indicateDiff = true;
        this.baseCls = 'data-table__row__cell';
        this.groupData = this.config.groupData;
        this.oldVal = null;
        this.init();
    }

    /**
     * 
     * @param {}
     */
    DataTableCell.prototype.init = function() {
        this.initEle();
    };

    /**
     * 
     * @param {}
     */
    DataTableCell.prototype.initEle = function() {
        var me = this;
        var types = me.config.CELL_TYPES;
        var type = types[me.config.cellType] || types[0];

        me.ele = dh.makeEle(type, {
            className: this.baseCls
        });

    };

    /**
     * 
     * @param {}
     */
    DataTableCell.prototype.setData = function(data) {
        this.data = data;
        this.updateData(data);
    };

    /**
     * 
     * @param {}
     */
    DataTableCell.prototype.updateData = function(newData) {
        var me = this;
        newData = newData || this.data;
        this.groupData.render(newData, me.ele);
    };

    /**
     * 
     * @param {}
     */
    DataTableCell.prototype.showStateDiffState = function(color) {
        this.ele.style.color = this.config.BASE_COLOR[color];
    };

    /**
     * Data Table Header Call
     * @class
     * @param {}
     */
    function DataTableHeader(config) {
        this.config = config || {};
        this.ele = null;
        this.config.baseCls = 'data-table__header';
        this.direction = 'ASC';
        this.arrows = null;
        this.content = null;
        this.data = this.config.data;
        this.headerData = this.config.headerData;
        this.width = this.headerData.width || this.config.width;
        this.enableSort = this.headerData.enableSort;
        this.isSorted = false;
        this.init();
    }

    /**
     * 
     * @param {}
     */
    DataTableHeader.prototype.init = function() {
        var me = this;
        this.ele = dh.makeEle('th', {
            className: this.config.baseCls + ' ' + 'unselectable',
            style: 'width: ' + me.width + '%'
        });

        this.content = dh.makeEle('div');
        this.content.innerHTML = this.headerData.groupName;
        this.ele.appendChild(this.content);

        if(this.enableSort) {
            this.initArrows();
        }
    };

    DataTableHeader.prototype.reset = function() {
        this.setIsSorted(false);
        this.setDirection('ASC');
    };

    DataTableHeader.prototype.setDirection = function(d) {
        this.direction = d;
        this.updateDirection();
    };

    DataTableHeader.prototype.updateDirection = function(newDir) {

    };

    DataTableHeader.prototype.initArrows = function() {
        this.arrowWrap = dh.makeEle('div', {className: 'data-table__header-arrow-container'});
        var up = dh.makeEle('div', { className: 'data-table__header-arrow--up' });
        var down = dh.makeEle('div', { className: 'data-table__header-arrow--down' });
        this.arrowWrap.appendChild(up);
        this.arrowWrap.appendChild(down);
        this.arrows = {
            up: up,
            down: down
        };
        this.ele.appendChild(this.arrowWrap);
    };

    DataTableHeader.prototype.setIsSorted = function(isSorted) {
        this.isSorted = isSorted;
        this.updateIsSorted();
    };

    DataTableHeader.prototype.updateIsSorted = function(newIsSorted) {

    };

    DataTableHeader.prototype.toggleDirection = function() {
        var d = this.direction;
        this.direction = d === 'ASC' ? 'DEC' : 'ASC';
        this.onToggleDirection();
        return this.direction;
    };

    DataTableHeader.prototype.onToggleDirection = function() {
        this.toggeleArrow();
    };

    DataTableHeader.prototype.resetArrows = function() {
        if(this.enableSort) {
            this.arrows.up.style.display = 'block';
            this.arrows.down.style.display = 'block';
        }
        
    };

    DataTableHeader.prototype.toggeleArrow = function() {
        var flag = this.direction == 'ASC';
        this.arrows.up.style.display = flag ? 'none' : 'block';
        this.arrows.down.style.display = !flag ? 'none' : 'block';
    };

    function initStyle() {
        var style = document.querySelector('style') || (function() {
            var s = document.createElement('style');
            document.head.appendChild(s);
            return s;
        })();
        var css = [
            '.data-table{width: 100%; font-family: arial;}',
            '.data-table-container{width: 100%;}',
            '.data-table__filter-input{padding: 10px;}',
            '.data-table__table{width: 100%}',
            // '.data-table__data-row:nth-child(odd){background:#f6f6f6;}',
            '.data-table__header{background: #efefef; padding:15px 10px; position: relative; text-align:left;}',
            '.data-table__row__cell{border: 1px solid #ccc; padding: 10px;}',
            '.data-table__heading-cell{background: #f6f6f6; padding: 10px; position: relative;}',
            '.data-table__header-arrow--up{cursor: pointer;width: 0px;height: 0px;border-left: 5px solid transparent;border-right: 5px solid transparent;border-bottom: 5px solid #fff;margin-bottom: 2px;}',
            '.data-table__header-arrow-container{background-color: #046591; border-radius: 12px;cursor: pointer;position: absolute;right: 5%;top: 50%;transform: translateY(-50%);width: 25px;height: 25px;display: flex;align-items: center;justify-content: center;flex-direction: column;}',
            '.data-table__header-arrow--down{cursor: pointer;width: 0px;height: 0px;border-left: 5px solid transparent;border-right: 5px solid transparent;border-top: 5px solid #fff;}',
            '.pagination,.pagination-btn-holder{display: flex;}',
            '.pagination{justify-content: space-between;padding: 10px 3px;flex-direction: row-reverse;}',
            '.pagination-btn{width: 30px; height: 30px; border-radius: 2px;border: 1px solid #ccc; text-align: center;line-height: 30px; cursor: pointer;}',
            '.pagination-btn:hover{background: #ccc;}',
            '.unselectable{-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}'
        ].join('');

        style.innerHTML += css;
    }
    initStyle();

    return DataTable;

})();